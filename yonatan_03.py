import datetime
import socket

PROXY_PORT = 9090

SERVER_IP = "54.71.128.194"
SERVER_PORT = 92

def ban_france(client_msg, client_sock):
    """
    BANNING FRANCE movies :)
    param: client_msg: the clients message
    param: client_sock: the clients socket
    type: client_msg: str
    type: client_sock: socket
    return: none

    """
    ban_msg = 'ERROR#"France is banned!"'

    if("france" in client_msg.lower()):
        client_sock.sendall(ban_msg.encode())

    
def fix_errors(server_msg, client_sock):
    """
    make server errors recognisable by client
    param: server_msg: the server message
    param: client_sock: the clients socket
    type: server_msg: str
    type: client_sock: socket
    return: none
    """

    if("SERVERERROR" in server_msg):    
        error_msg = "ERROR#" + server_msg[server_msg.find('"'):] 
        client_sock.sendall(error_msg.encode())
        
def check_error(server_msg):
    """
    check if theres errors
    param: server_msg: the server message
    type: server_msg: str
    return: if theres an error
    rtype: bool
    """

    return "ERROR" in server_msg

def check_years(client_msg):
    """
    if start year is bigger than end year switch them for server
    param: client_msg: the clients message
    type: client_msg: str
    return: fixed msg / original msg
    rtype: str
    """

    years = client_msg.split("&")[1].split(":")[1].split("-")
    year1 = int(years[0])
    year2 = int(years[1])
    if(year1 > year2):
        edit_index2 = client_msg.find("&country")
        edit_index1 = client_msg.find("year:") + 5
        client_msg = client_msg[:edit_index1] + str(year2) + "-" + str(year1) + client_msg[edit_index2:]

    return client_msg

def fix_image(server_msg):
    """
    fix the image url
    param: server_msg: the server message
    type: server_msg: str
    return: fixed msg
    rtype: str
    """
    
    return server_msg.replace("jpg", ".jpg")

def proxy():
    """
    a working proxy
    """
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as listening_sock:
        proxy_address = ('', PROXY_PORT)
        listening_sock.bind(proxy_address)

        while True:
            listening_sock.listen(1)
            client_soc, client_address = listening_sock.accept()
            client_msg = client_soc.recv(1024)
            client_msg = client_msg.decode()
            
            client_msg = check_years(client_msg)
            ban_france(client_msg, client_soc)

            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                server_address = (SERVER_IP, SERVER_PORT)
                sock.connect(server_address)
                sock.sendall(client_msg.encode())
                server_msg = sock.recv(1024)
                server_msg = server_msg.decode()

                if(check_error(server_msg)):
                    fix_errors(server_msg, client_soc)
                else:
                    server_msg = fix_image(server_msg)

            client_soc.sendall(server_msg.encode())
     

def main():
    proxy()
    


if __name__ == "__main__":
    main()
