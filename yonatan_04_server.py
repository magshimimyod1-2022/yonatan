import socket

SERVER_PORT = 65432

def welcome(client_sock):
    client_sock.sendall("              Welocme to Pink Floyd server\n\n".encode())

def get_album_list():
    return "get_album_list"

def get_album_songs(album_name):
    return "get_album_songs"

def get_song_length(song_name):
    return "get_song_length"

def get_song_lyrics(song_name):
    return "get_song_lyrics"

def get_song_album(song_name):
    return "get_song_album"

def find_song_name(song_name):
    return "find_song_name"

def find_song_by_lyrics(lyrics):
    return "find_song_by_lyrics"

    
def server():
    """
    a server
    """
    actions = {"welcome": welcome, "100": get_album_list, "200": get_album_songs, "300": get_song_length, 
               "400": get_song_lyrics, "500": get_song_album, "600": find_song_name, "700": find_song_by_lyrics}
    Quit = False
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as listening_sock:
        server_address = ('', SERVER_PORT)
        listening_sock.bind(server_address)
    
        while not Quit:
            listening_sock.listen(1)
            client_soc, client_address = listening_sock.accept()
            actions["welcome"](client_soc)
            end_conv = False

            while end_conv is False:
                client_msg = client_soc.recv(1024).decode()
                if (client_msg[:3] == "800" or client_msg == ""):
                    end_conv = True 
                else:
                    if client_msg[:3] == "100": 
                        server_msg = actions[client_msg[:3]]()
                    else:
                        server_msg = actions[client_msg[:3]](client_msg[client_msg.find("param") + 6:])

                    client_soc.sendall(server_msg.encode())

    
def main():
    server()


if __name__ == "__main__":
    main()
