from distutils import filelist
import requests

def extract_password_from_site():
    """
    return: the password
    rtype: string
    """

    URL = "http://webisfun.cyber.org.il/nahman/files"

    password = ''
    for file_url in range(11,35):
        file_url = URL + "/file" + str(file_url) + ".nfo"
        r = requests.get(file_url)
        password += str(r.content)[101]

    return password

def find_most_common_words(path, words):
    """
    param: path: the path to the file
    param: words: the amount of words
    type: path: str
    type: words: int
    return: the sentence
    rtype: str
    """

    URL = "http://webisfun.cyber.org.il/nahman/final_phase/"
    sentence = ''
    r = requests.get(URL + path)
    word_list = r.text.split(" ")
    for word in range(words): 
        word1 = max(set(word_list), key = word_list.count)
        sentence += word1 + " "
        word_list = [i for i in word_list if i != word1]

    return sentence
    



def main():
    choise = int(input())

    if choise == 1:
       print(extract_password_from_site())
    elif choise == 2:
        print(find_most_common_words("words.txt", 6))
if __name__ == "__main__":
    main()
